<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LearndaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Learnday::factory()->create([
            'title' => 'Hetfő',
            'date' => '2024-01-22'
            
        ]);
        \App\Models\Learnday::factory()->create([
            'title' => 'Kedd',
            'date' => '2024-01-23'
            
        ]);
        \App\Models\Learnday::factory()->create([
            'title' => 'Szerda',
            'date' => '2024-01-24'
            
        ]);
        \App\Models\Learnday::factory()->create([
            'title' => 'Csütörtök',
            'date' => '2024-01-25'
            
        ]);
    }
}
