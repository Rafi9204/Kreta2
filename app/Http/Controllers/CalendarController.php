<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {
        $calendardays = Calendar::all();
        return view("views.home", ['calendarsdays' => $calendardays]);
    }
}
