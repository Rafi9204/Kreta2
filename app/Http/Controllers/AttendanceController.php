<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreattendanceRequest;
use App\Http\Requests\UpdateattendanceRequest;
use App\Models\attendance;
use App\Models\Learnday;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store($learnday)
    {
        dd($learnday);
        
    }

    /**
     * Display the specified resource.
     */
    public function show(attendance $attendance)
    {
        $attendance = attendance::find($attendance);
        return view('attendances.show', ['attendances' => $attendance ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(attendance $attendance)
    {
        

        $attendance = attendance::find($attendance);
        return view('attendance.edit', ['attendances' => $attendance]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateattendanceRequest $request, attendance $attendance)
    {
        $attendance->update($request->all());

        return redirect()->route('attendances.index')->with('success', 'The learn day was updated successfully.');
    }
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(attendance $attendance)
    {
        $attendance = attendance::find($attendance);
        $attendance->delete();
        return back()->with('success', 'Successfully deleted the learn day: ' . $attendance->learnday->course->title . '.');
    }

    // saját funkció

  /*   public function class_index($learndays)
    {
        return view('attendances.index', compact('learndays'));
    } */

    public function massupdate(Request $request, Learnday $learnday)
    {
        $students = collect();
        $studentsNew = $learnday->attendances; 
        
        
        foreach ($studentsNew as $key => $value) {
            $students->push($value->student);
        }
        
        foreach ($students as $key => $student) {
            $studentStatus = $student->attendances->where('learnday_id', $learnday->id)->first()->status; 

            
            if ( array_key_exists('status'.$student->id, $request->all() )  ) {
                $studentInputStatus = $request->all()['status'.$student->id]; 

                if ($studentStatus != $studentInputStatus) 
                {                    
                    
                    Attendance::where('student_id', $student->id)->where('learnday_id', $learnday->id)->update([
                        'status' => $studentInputStatus,
                    ]);
                }
            }
        }
        return back()->with('success', 'Successfully saved the changes.');
    }

}
