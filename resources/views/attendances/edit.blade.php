@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="card border-secondary bg-dark">
                <div class="card-body text-white">
                    <h4 class="card-title">Edit a Learn Day named: {{ $attendances->status }}</h4>
                    <p class="card-text text-danger">Inputs marked with * shall be filled.</p>

                    @if ($errors->any())
                        <div class="mb-3 mt-3">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                                <strong>Holy guacamole!</strong>

                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                            <strong>Holy guacamole!</strong>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <form action="{{ route('learndays.update', $attendances ) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label>
                            <input type="text" name="status" id="status"
                                class="form-control @if ($errors->has('status')) border border-danger @endif"
                                placeholder="status of the Day" aria-describedby="helpId"
                                value="{{ old('status', $attendances->status) }}">

                            @if ($errors->has('name'))
                                <small class="text-danger">{{ $errors->first('title') }}</small>
                            @else
                                <small id="helpId" class="text-white">The name of the course to be created.</small>
                            @endif

                        </div>
                        <div class="mb-3">
                            <label for="date" class="form-label">Date</label>
                            <input type="text" name="date" id="date" class="form-control"
                                placeholder="0000-00-00" aria-describedby="helpId" maxlength="255"
                                value="{{ old('date', $learndays->date) }}">
                            <small id="helpId" class="text-white">Date of the Learn Day</small>
                        </div>
                        <div class="mb-3">
                            <label for="course" class="form-label">Class</label>
                            <select class="form-select" aria-label="Default select example" name="course_id">
                                <option hidden>Classes</option>
                                @foreach ($courses as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <small id="helpId" class="text-white">For which class the learn day is</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

@endsection
